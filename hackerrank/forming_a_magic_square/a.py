#!/usr/bin/env python3

# Exercise -> https://www.hackerrank.com/challenges/magic-square-forming/problem
# Key concepts:
# -> https://en.wikipedia.org/wiki/Magic_square

from typing import Iterable, Dict
from itertools import combinations, chain

IntIterable = Iterable[int]
IntMatrix = Iterable[IntIterable]
IterableIntMatrix = Iterable[IntMatrix]

# Step 1 - Creating solution space
# The absolutely easiest way to do this is to get all the possible magic
# squares for a 3x3 square. After getting my PhD in magic squares, for a 3x3
# square we only have 8 possible solutions, which in reality is the same
# solution rotated and mirrored as many times as necessary to get the 8
# possible configurations- Instead of typing all the possible configurations,
# we'll generate one and rotate/mirror as required.

# The first step here is to generate a valid configuration, we can do this by
# calculating all the possible combinations of three digits that equal the
# magic constant. The magic constant is calculated by M = n(n^2 + 1)/2
n = 3

# For normal magic squares of orders n = 3, 4, 5, 6, 7, and 8, the magic
# constants are, respectively: 15, 34, 65, 111, 175, and 260
magic_constant = (n * ((n * n) + 1))/2

# Another bit of information that could be very useful is that for odd magic
# squares, the center value can be calculated by (n^2 + 1)/2
center_value = ((n * n) + 1)/2

# Instead of looking up the correct magic square we can discover it with a bit
# of code. Calculating all possible combinations of three digits (within the
# digit constraint which is 1 -> n^2) of which the sum equals to the magic
# constant


def calc_valid_combs(n: int, magic_constant: float) -> IntMatrix:
    print("Valid combinations")
    combs = combinations(range(1, (n * n) + 1), n)
    good_combs = []  # type: IntMatrix
    for item in combs:
        if item[0] + item[1] + item[2] == magic_constant:
            good_combs.append(item)

    print(good_combs)
    return good_combs


valid_combs = calc_valid_combs(n, magic_constant)

# Now we have a list of valid combinations:
# [(1, 5, 9), (1, 6, 8), (2, 4, 9), (2, 5, 8), (2, 6, 7), (3, 4, 8), (3, 5, 7), (4, 5, 6)]
# With careful and lengthy observation we can come to a couple of conclusions:
# * The number in the center should be in the list 4 times
# * Each of the four corner numbers should be in the list twice


def calc_num_pos(combs: IntMatrix) -> None:
    print("Calculate number positions")
    occur = dict.fromkeys(range(1, (n * n) + 1), 0)  # type: Dict[int, int]
    # Create occurence dictionary
    for row in combs:
        for item in row:
            occur[item] += 1
    # Iterate dictionary and select numbers
    for key in occur:
        if occur[key] == 4:
            print("Center value: ", key)
        if occur[key] == 3:
            print("Corner value: ", key)


calc_num_pos(valid_combs)

# We end up with the following information:
# Corner value:  2
# Corner value:  4
# Center value:  5
# Corner value:  6
# Corner value:  8
# Combined with the information from the valid combinations, this gives us the
# following partial solution:
# 2 _ 4
# _ 5 _
# 6 _ 8
# Now we just need to fill in the gaps
# 2 9 4
# 7 5 3
# 6 1 8
# With this valid magic square, the only thing that remains is to mirror it and
# rotate both squares three times to get all eight valid magic squares
valid_square = [[2, 9, 4], [7, 5, 3], [6, 1, 8]]


def rotate_matrix_cw(matrix: IntMatrix) -> IntMatrix:
    rev_list = list(reversed(matrix))  # type: IntMatrix
    rotated = zip(*rev_list)
    new_matrix = list(rotated)
    return new_matrix


def mirror_matrix_h(matrix: IntMatrix) -> IntMatrix:
    new_matrix = []  # type: IntMatrix
    for row in matrix:
        rev_list = list(reversed(row))  # type: IntIterable
        new_matrix.append(rev_list)
    return new_matrix


def gen_valid_squares(valid_square: IntMatrix) -> IterableIntMatrix:
    print("Generating valid squares")
    valid_squares = []  # type: IterableIntMatrix
    orig_square = valid_square
    mirror_orig_square = mirror_matrix_h(orig_square)
    valid_squares.append(orig_square)
    valid_squares.append(mirror_orig_square)
    prev_square = orig_square
    for _ in range(1, 4):
        prev_square = rotate_matrix_cw(prev_square)
        valid_squares.append(prev_square)
    prev_square = mirror_orig_square
    for _ in range(1, 4):
        prev_square = rotate_matrix_cw(prev_square)
        valid_squares.append(prev_square)
    return valid_squares


def print_square(matrix: IntMatrix) -> None:
    print("Printing matrix")
    for row in matrix:
        print(row)


valid_squares = gen_valid_squares(valid_square)
print("Valid squares: ", valid_squares)
for square in valid_squares:
    print_square(square)

# Once we have all the valid squares, it's just a matter of comparing the
# distance/cost from the given square to the valid ones.
# The problem is trivial if we hardcode the valid squares but we might as well
# take the long way to the solution.

s = [[5, 3, 4], [1, 5, 8], [6, 4, 2]]


def calc_cost(src: IntMatrix, trg: IntMatrix) -> int:
    cost = 0
    src_list = list(chain(*src))
    trg_list = list(chain(*trg))
    for i in range(0, len(src_list)):
        cost += abs(src_list[i] - trg_list[i])
    return cost


min_cost = n ** 4
best_square = []  # type: IntMatrix
for square in valid_squares:
    print("Square transformation costs")
    print_square(s)
    print_square(square)
    cost = calc_cost(s, square)
    print(cost)
    if min_cost > cost:
        min_cost = cost
        best_square = square

print("Best square")
print_square(best_square)
print("Cost: ", min_cost)

def formingMagicSquare(s: IntMatrix):
    valid_squares = [[[2, 9, 4], [7, 5, 3], [6, 1, 8]], [[4, 9, 2], [3, 5, 7], [8, 1, 6]], [(6, 7, 2), (1, 5, 9), (8, 3, 4)], [(8, 1, 6), (3, 5, 7), (4, 9, 2)], [(4, 3, 8), (9, 5, 1), (2, 7, 6)], [(8, 3, 4), (1, 5, 9), (6, 7, 2)], [(6, 1, 8), (7, 5, 3), (2, 9, 4)], [(2, 7, 6), (9, 5, 1), (4, 3, 8)]]
    min_cost = 99
    for square in valid_squares:
        cost = calc_cost(s, square)
        if min_cost > cost:
            min_cost = cost
    return min_cost


print(formingMagicSquare(s))
